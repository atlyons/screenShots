var url = "https://www.google.com";
var env = url; // url we want our tests to launch to


const SCREENSHOT_PATH = "./screenshots/";
const BINPATH = './lib/node_modules/nightwatch/bin/';



// we use a nightwatch.conf.js file so we can include comments and helper functions
module.exports = {
  "src_folders": [
    "test/e2e"// Where you are storing your Nightwatch e2e tests
  ],
  "custom_commands_path" : "",
  "custom_assertions_path" : "",
  "page_objects_path" : "./lib/pages",
  "globals_path" : "globals.js",
  "globals" : "",
  "output_folder": "./lib/reports",
  "selenium": { // downloaded by selenium-download module (see readme)
    "start_process": true, // tells nightwatch to start/stop the selenium process
    "server_path": "./lib/drivers/selenium-server-standalone-3.4.0.jar",
    "host": "127.0.0.1",
    "port": 4444, // standard selenium port
    "cli_args": { // chromedriver is downloaded by selenium-download (see readme)
      "webdriver.chrome.driver" : "./lib/drivers/chromedriver.exe",
      "webdriver.gecko.driver" : "./lib/drivers/geckodriver.exe",
      "webdriver.safari.driver" : "",
      "webdriver.ie.driver" : "./lib/drivers/IEDriverServer.exe",
      "webdriver.edge.driver" : "./lib/drivers/MicrosoftWebDriver.exe"
    }
  },
  "test_settings": {
    "default": {
      "launch_url": env,
      "screenshots": {
        "enabled": true, // if you want to keep screenshots
        "on_failure" : false, // if you want screenshots for failed tests
        "on_error" : false, // if you want screenshots for errors while test is running (off while constructing tests as it results in a lot of screenshots)
        "path": './screenshots' // save screenshots here
      },
      "globals": {
        "waitForConditionTimeout": 5000 // sometimes internet is slow so wait.
      },
      "desiredCapabilities": { // use Chrome as the default browser for tests
        "browserName": "chrome",
      }
    },
    "chrome": {
      "desiredCapabilities": {
        "browserName": "chrome",
        "javascriptEnabled": true, // turn off to test progressive enhancement
      }
    },
    "safari":{
        "desiredCapabilities": {
        "browserName": "safari",
        "javascriptEnabled": true,
        "acceptSslCerts": true,
        "marionette": true
      }
    },
    "firefox":{
        "desiredCapabilities": {
        "browserName": "firefox",
        "javascriptEnabled": true,
        "acceptSslCerts": true,
        "marionette": true
      }
    },
    "ie":{
        "desiredCapabilities": {
        "browserName": "ie",
        "javascriptEnabled": true,
        "acceptSslCerts": true,
        "marionette": true
      }
    },
    "edge":{
        "desiredCapabilities": {
        "browserName": "MicrosoftEdge",
        "javascriptEnabled": true,
        "acceptSslCerts": true,
        "marionette": true
      }
    },
      "ios" : {
      "selenium_start_process": false,
      "selenium_port" : 4723,
      "selenium_host" : "127.0.0.1",
      "silent": true,
      "desiredCapabilities" : {
      "browserName" : "Safari",
      "platformName" : "iOS",
      "platformVersion" : "10.2",
      "deviceName" : "iPhone 6s Plus"
      }
    },
      "android" : {
          "selenium_start_process": false,
          "selenium_port" : 4723,
          "selenium_host" : "127.0.0.1",
          "silent": true,
          "desiredCapabilities": {
              "browserName": "chrome",
              "platformName": "Android",
              "platformVersion": "4.4",
              "device": "Android",
              "deviceName": "Nexus_6_API_25",
              "avd": "Nexus_6_API_25"
      }
    },
      "test" : {
          "selenium_start_process": false,
          "selenium_port" : 4723,
          "selenium_host" : "127.0.0.1",
          "silent": true,
          "desiredCapabilities": {
              "browser": "chrome",
              "platformName": "Android",
              "platformVersion": "7.0",
              "device": "Android",
              "deviceName": "Nexus_6_API_25",
              "avd": "Nexus_6_API_25"
      }
    }
  }
}


/**
 * selenium-download does exactly what it's name suggests;
 * downloads (or updates) the version of Selenium (& chromedriver)
 * on your localhost where it will be used by Nightwatch.
 /the following code checks for the existence of `selenium.jar` before trying to run our tests.
 */

require('fs').stat(BINPATH + 'selenium.jar', function (err, stat) { // got it?
  if (err || !stat || stat.size < 1) {
    require('selenium-download').ensure(BINPATH, function(error) {
      if (error) throw new Error(error); // no point continuing so exit!
      console.log('✔ Selenium & Chromedriver downloaded to:', BINPATH);
    });
  }
});

function padLeft (count) { // theregister.co.uk/2016/03/23/npm_left_pad_chaos/
  return count < 10 ? '0' + count : count.toString();
}

var FILECOUNT = 0; // "global" screenshot file count
/**
 * The default is to save screenshots to the root of your project even though
 * there is a screenshots path in the config object above! ... so we need a
 * function that returns the correct path for storing our screenshots.
 * While we're at it, we are adding some meta-data to the filename, specifically
 * the Platform/Browser where the test was run and the test (file) name.
 */
function imgpath (browser) {
  var a = browser.options.desiredCapabilities;
  var meta = [a.platform];
  meta.push(a.browserName ? a.browserName : 'any');
  meta.push(a.version ? a.version : 'any');
  meta.push(a.name); // this is the test filename so always exists.
  var metadata = meta.join('~').toLowerCase().replace(/ /g, '');
  return SCREENSHOT_PATH + metadata + '_' + padLeft(FILECOUNT++) + '_';
}

module.exports.imgpath = imgpath;
module.exports.SCREENSHOT_PATH = SCREENSHOT_PATH;
