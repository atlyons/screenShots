module.exports = {
    "URL_5 SCREENSHOT..." : function (browser) {
        browser
            .page.function().windowSize()        
            .url(url_5)
            .pause(500)
            .element('css selector', 'title', function(result){
                if (result.value && result.value.ELEMENT) {
                    browser
                    .assert.urlEquals(url_5, 'url_5 - Page url equals url_5')
                    .pause(500)            
                    .waitForElementVisible('body', 2500, 'url_5 - Body was displayed')            
                    .saveScreenshot('./screenshots/url_5.jpeg')
                .end();
                } else {
                    // URL HAS NOT BEEN ENTERED INTO GLOBALS.JS
                    console.log(consoleWarnColor, "WARN:                                                      ")
                    console.log(consoleTextColor, "URL_5 - No url entered for #5 in 'globals.js' file         ")
                    console.log(consoleTextColor, "        Add a url to #5 and rerun this test to see results ")                                        
                    browser.end();
                }
            })
      .end();
    }
};