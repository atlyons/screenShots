module.exports = {
    "URL_6 SCREENSHOT..." : function (browser) {
        browser
            .page.function().windowSize()        
            .url(url_6)
            .pause(500)
            .element('css selector', 'title', function(result){
                if (result.value && result.value.ELEMENT) {
                    browser
                    .assert.urlEquals(url_6, 'url_6 - Page url equals url_6')
                    .pause(500)            
                    .waitForElementVisible('body', 2500, 'url_6 - Body was displayed')            
                    .saveScreenshot('./screenshots/url_6.jpeg')
                .end();
                } else {
                    // URL HAS NOT BEEN ENTERED INTO GLOBALS.JS
                    console.log(consoleWarnColor, "WARN:                                                      ")
                    console.log(consoleTextColor, "URL_6 - No url entered for #6 in 'globals.js' file         ")
                    console.log(consoleTextColor, "        Add a url to #7 and rerun this test to see results ")                                        
                    browser.end();
                }
            })
      .end();
    }
};