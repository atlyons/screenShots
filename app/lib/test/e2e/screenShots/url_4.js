module.exports = {
    "URL_4 SCREENSHOT..." : function (browser) {
        browser
            .page.function().windowSize()        
            .url(url_4)
            .pause(500)
            .element('css selector', 'title', function(result){
                if (result.value && result.value.ELEMENT) {
                    browser
                    .assert.urlEquals(url_4, 'url_4 - Page url equals url_4')
                    .pause(500)            
                    .waitForElementVisible('body', 2500, 'url_4 - Body was displayed')            
                    .saveScreenshot('./screenshots/url_4.jpeg')
                .end();
                } else {
                    // URL HAS NOT BEEN ENTERED INTO GLOBALS.JS
                    console.log(consoleWarnColor, "WARN:                                                      ")
                    console.log(consoleTextColor, "URL_4 - No url entered for #4 in 'globals.js' file         ")
                    console.log(consoleTextColor, "        Add a url to #7 and rerun this test to see results ")                                        
                    browser.end();
                }
            })
      .end();
    }
};