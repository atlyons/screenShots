module.exports = {
    "URL_11 SCREENSHOT..." : function (browser) {
        browser
            .page.function().windowSize()        
            .url(url_11)
            .pause(500)
            .element('css selector', 'title', function(result){
                if (result.value && result.value.ELEMENT) {
                    browser
                    .assert.urlEquals(url_11, 'url_11 - Page url equals url_11')
                    .pause(500)            
                    .waitForElementVisible('body', 2500, 'url_11 - Body was displayed')            
                    .saveScreenshot('./screenshots/url_11.jpeg')
                .end();
                } else {
                    // URL HAS NOT BEEN ENTERED INTO GLOBALS.JS
                    console.log(consoleWarnColor, "WARN:                                                      ")
                    console.log(consoleTextColor, "URL_11 - No url entered for #11 in 'globals.js' file         ")
                    console.log(consoleTextColor, "        Add a url to #11 and rerun this test to see results ")                                        
                    browser.end();
                }
            })
      .end();
    }
};