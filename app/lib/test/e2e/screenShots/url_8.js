module.exports = {
    "URL_8 SCREENSHOT..." : function (browser) {
        browser
            .page.function().windowSize()        
            .url(url_8)
            .pause(500)
            .element('css selector', 'title', function(result){
                if (result.value && result.value.ELEMENT) {
                    browser
                    .assert.urlEquals(url_8, 'url_8 - Page url equals url_8')
                    .pause(500)            
                    .waitForElementVisible('body', 2500, 'url_8 - Body was displayed')            
                    .saveScreenshot('./screenshots/url_8.jpeg')
                .end();
                } else {
                    // URL HAS NOT BEEN ENTERED INTO GLOBALS.JS
                    console.log(consoleWarnColor, "WARN:                                                      ")
                    console.log(consoleTextColor, "URL_8 - No url entered for #8 in 'globals.js' file         ")
                    console.log(consoleTextColor, "        Add a url to #8 and rerun this test to see results ")                                        
                    browser.end();
                }
            })
      .end();
    }
};