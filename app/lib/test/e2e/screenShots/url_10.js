module.exports = {
    "URL_10 SCREENSHOT..." : function (browser) {
        browser
            .page.function().windowSize()        
            .url(url_10)
            .pause(500)
            .element('css selector', 'title', function(result){
                if (result.value && result.value.ELEMENT) {
                    browser
                    .assert.urlEquals(url_10, 'url_10 - Page url equals url_10')
                    .pause(500)            
                    .waitForElementVisible('body', 2500, 'url_10 - Body was displayed')            
                    .saveScreenshot('./screenshots/url_10.jpeg')
                .end();
                } else {
                    // URL HAS NOT BEEN ENTERED INTO GLOBALS.JS
                    console.log(consoleWarnColor, "WARN:                                                      ")
                    console.log(consoleTextColor, "URL_10 - No url entered for #10 in 'globals.js' file         ")
                    console.log(consoleTextColor, "        Add a url to #10 and rerun this test to see results ")                                        
                    browser.end();
                }
            })
      .end();
    }
};