module.exports = {
    "URL_20 SCREENSHOT..." : function (browser) {
        browser
            .page.function().windowSize()        
            .url(url_20)
            .pause(500)
            .element('css selector', 'title', function(result){
                if (result.value && result.value.ELEMENT) {
                    browser
                    .assert.urlEquals(url_20, 'url_20 - Page url equals url_20')
                    .pause(500)            
                    .waitForElementVisible('body', 2500, 'url_20 - Body was displayed')            
                    .saveScreenshot('./screenshots/url_20.jpeg')
                .end();
                } else {
                    // URL HAS NOT BEEN ENTERED INTO GLOBALS.JS
                    console.log(consoleWarnColor, "WARN:                                                      ")
                    console.log(consoleTextColor, "URL_20 - No url entered for #20 in 'globals.js' file         ")
                    console.log(consoleTextColor, "        Add a url to #20 and rerun this test to see results ")                                        
                    browser.end();
                }
            })
      .end();
    }
};