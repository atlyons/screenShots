module.exports = {
    "URL_16 SCREENSHOT..." : function (browser) {
        browser
            .page.function().windowSize()        
            .url(url_16)
            .pause(500)
            .element('css selector', 'title', function(result){
                if (result.value && result.value.ELEMENT) {
                    browser
                    .assert.urlEquals(url_16, 'url_16 - Page url equals url_16')
                    .pause(500)            
                    .waitForElementVisible('body', 2500, 'url_16 - Body was displayed')            
                    .saveScreenshot('./screenshots/url_16.jpeg')
                .end();
                } else {
                    // URL HAS NOT BEEN ENTERED INTO GLOBALS.JS
                    console.log(consoleWarnColor, "WARN:                                                      ")
                    console.log(consoleTextColor, "URL_16 - No url entered for #16 in 'globals.js' file         ")
                    console.log(consoleTextColor, "        Add a url to #16 and rerun this test to see results ")                                        
                    browser.end();
                }
            })
      .end();
    }
};