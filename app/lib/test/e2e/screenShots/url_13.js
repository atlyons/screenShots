module.exports = {
    "URL_13 SCREENSHOT..." : function (browser) {
        browser
            .page.function().windowSize()        
            .url(url_13)
            .pause(500)
            .element('css selector', 'title', function(result){
                if (result.value && result.value.ELEMENT) {
                    browser
                    .assert.urlEquals(url_13, 'url_13 - Page url equals url_13')
                    .pause(500)            
                    .waitForElementVisible('body', 2500, 'url_13 - Body was displayed')            
                    .saveScreenshot('./screenshots/url_13.jpeg')
                .end();
                } else {
                    // URL HAS NOT BEEN ENTERED INTO GLOBALS.JS
                    console.log(consoleWarnColor, "WARN:                                                      ")
                    console.log(consoleTextColor, "URL_13 - No url entered for #13 in 'globals.js' file         ")
                    console.log(consoleTextColor, "        Add a url to #13 and rerun this test to see results ")                                        
                    browser.end();
                }
            })
      .end();
    }
};