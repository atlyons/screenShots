module.exports = {
    "URL_7 SCREENSHOT..." : function (browser) {
        browser
            .page.function().windowSize()
            .url(url_7)
            .pause(500)
            .element('css selector', 'title', function(result){
                // IF URL HAS BEEN ENTERED INTO GLOBALS.JS
                if (result.value && result.value.ELEMENT) {
                    browser
                    .assert.urlEquals(url_7, 'url_7 - Page url equals url_7')
                    .pause(500)            
                    .waitForElementVisible('body', 2500, 'url_7 - Body was displayed')            
                    .saveScreenshot('./screenshots/url_7.jpeg')
                .end();
                } else {
                    // URL HAS NOT BEEN ENTERED INTO GLOBALS.JS
                    console.log(consoleWarnColor, "WARN:                                                      ")
                    console.log(consoleTextColor, "URL_7 - No url entered for #7 in 'globals.js' file         ")
                    console.log(consoleTextColor, "        Add a url to #7 and rerun this test to see results ")                                        
                    browser.end();
                }
            })
      .end();
    }
};