module.exports = {
    "URL_18 SCREENSHOT..." : function (browser) {
        browser
            .page.function().windowSize()        
            .url(url_18)
            .pause(500)
            .element('css selector', 'title', function(result){
                if (result.value && result.value.ELEMENT) {
                    browser
                    .assert.urlEquals(url_18, 'url_18 - Page url equals url_18')
                    .pause(500)            
                    .waitForElementVisible('body', 2500, 'url_18 - Body was displayed')            
                    .saveScreenshot('./screenshots/url_18.jpeg')
                .end();
                } else {
                    // URL HAS NOT BEEN ENTERED INTO GLOBALS.JS
                    console.log(consoleWarnColor, "WARN:                                                      ")
                    console.log(consoleTextColor, "URL_18 - No url entered for #18 in 'globals.js' file         ")
                    console.log(consoleTextColor, "        Add a url to #18 and rerun this test to see results ")                                        
                    browser.end();
                }
            })
      .end();
    }
};