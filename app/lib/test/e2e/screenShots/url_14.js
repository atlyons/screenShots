module.exports = {
    "URL_14 SCREENSHOT..." : function (browser) {
        browser
            .page.function().windowSize()        
            .url(url_14)
            .pause(500)
            .element('css selector', 'title', function(result){
                if (result.value && result.value.ELEMENT) {
                    browser
                    .assert.urlEquals(url_14, 'url_14 - Page url equals url_14')
                    .pause(500)            
                    .waitForElementVisible('body', 2500, 'url_14 - Body was displayed')            
                    .saveScreenshot('./screenshots/url_14.jpeg')
                .end();
                } else {
                    // URL HAS NOT BEEN ENTERED INTO GLOBALS.JS
                    console.log(consoleWarnColor, "WARN:                                                      ")
                    console.log(consoleTextColor, "URL_14 - No url entered for #14 in 'globals.js' file         ")
                    console.log(consoleTextColor, "        Add a url to #14 and rerun this test to see results ")                                        
                    browser.end();
                }
            })
      .end();
    }
};