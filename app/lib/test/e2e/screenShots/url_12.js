module.exports = {
    "URL_12 SCREENSHOT..." : function (browser) {
        browser
            .page.function().windowSize()        
            .url(url_12)
            .pause(500)
            .element('css selector', 'title', function(result){
                if (result.value && result.value.ELEMENT) {
                    browser
                    .assert.urlEquals(url_12, 'url_12 - Page url equals url_12')
                    .pause(500)            
                    .waitForElementVisible('body', 2500, 'url_12 - Body was displayed')            
                    .saveScreenshot('./screenshots/url_12.jpeg')
                .end();
                } else {
                    // URL HAS NOT BEEN ENTERED INTO GLOBALS.JS
                    console.log(consoleWarnColor, "WARN:                                                      ")
                    console.log(consoleTextColor, "URL_12 - No url entered for #12 in 'globals.js' file         ")
                    console.log(consoleTextColor, "        Add a url to #12 and rerun this test to see results ")                                        
                    browser.end();
                }
            })
      .end();
    }
};