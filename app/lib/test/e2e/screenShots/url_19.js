module.exports = {
    "URL_19 SCREENSHOT..." : function (browser) {
        browser
            .page.function().windowSize()        
            .url(url_19)
            .pause(500)
            .element('css selector', 'title', function(result){
                if (result.value && result.value.ELEMENT) {
                    browser
                    .assert.urlEquals(url_19, 'url_19 - Page url equals url_19')
                    .pause(500)            
                    .waitForElementVisible('body', 2500, 'url_19 - Body was displayed')            
                    .saveScreenshot('./screenshots/url_19.jpeg')
                .end();
                } else {
                    // URL HAS NOT BEEN ENTERED INTO GLOBALS.JS
                    console.log(consoleWarnColor, "WARN:                                                      ")
                    console.log(consoleTextColor, "URL_19 - No url entered for #19 in 'globals.js' file         ")
                    console.log(consoleTextColor, "        Add a url to #19 and rerun this test to see results ")                                        
                    browser.end();
                }
            })
      .end();
    }
};