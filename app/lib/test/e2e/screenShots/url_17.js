module.exports = {
    "URL_17 SCREENSHOT..." : function (browser) {
        browser
            .page.function().windowSize()        
            .url(url_17)
            .pause(500)
            .element('css selector', 'title', function(result){
                if (result.value && result.value.ELEMENT) {
                    browser
                    .assert.urlEquals(url_17, 'url_17 - Page url equals url_17')
                    .pause(500)            
                    .waitForElementVisible('body', 2500, 'url_17 - Body was displayed')            
                    .saveScreenshot('./screenshots/url_17.jpeg')
                .end();
                } else {
                    // URL HAS NOT BEEN ENTERED INTO GLOBALS.JS
                    console.log(consoleWarnColor, "WARN:                                                      ")
                    console.log(consoleTextColor, "URL_17 - No url entered for #17 in 'globals.js' file         ")
                    console.log(consoleTextColor, "        Add a url to #17 and rerun this test to see results ")                                        
                    browser.end();
                }
            })
      .end();
    }
};