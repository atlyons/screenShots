module.exports = {
    "URL_15 SCREENSHOT..." : function (browser) {
        browser
            .page.function().windowSize()        
            .url(url_15)
            .pause(500)
            .element('css selector', 'title', function(result){
                if (result.value && result.value.ELEMENT) {
                    browser
                    .assert.urlEquals(url_15, 'url_15 - Page url equals url_15')
                    .pause(500)            
                    .waitForElementVisible('body', 2500, 'url_15 - Body was displayed')            
                    .saveScreenshot('./screenshots/url_15.jpeg')
                .end();
                } else {
                    // URL HAS NOT BEEN ENTERED INTO GLOBALS.JS
                    console.log(consoleWarnColor, "WARN:                                                      ")
                    console.log(consoleTextColor, "URL_15 - No url entered for #15 in 'globals.js' file         ")
                    console.log(consoleTextColor, "        Add a url to #15 and rerun this test to see results ")                                        
                    browser.end();
                }
            })
      .end();
    }
};