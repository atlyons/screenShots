module.exports = {
    "URL_9 SCREENSHOT..." : function (browser) {
        browser
            .page.function().windowSize()        
            .url(url_9)
            .pause(500)
            .element('css selector', 'title', function(result){
                if (result.value && result.value.ELEMENT) {
                    browser
                    .assert.urlEquals(url_9, 'url_9 - Page url equals url_9')
                    .pause(500)            
                    .waitForElementVisible('body', 2500, 'url_9 - Body was displayed')            
                    .saveScreenshot('./screenshots/url_9.jpeg')
                .end();
                } else {
                    // URL HAS NOT BEEN ENTERED INTO GLOBALS.JS
                    console.log(consoleWarnColor, "WARN:                                                      ")
                    console.log(consoleTextColor, "URL_9 - No url entered for #9 in 'globals.js' file         ")
                    console.log(consoleTextColor, "        Add a url to #9 and rerun this test to see results ")                                        
                    browser.end();
                }
            })
      .end();
    }
};