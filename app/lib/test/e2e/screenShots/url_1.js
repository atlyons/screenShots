module.exports = {
    "URL_1 SCREENSHOT..." : function (browser) {
        browser
            .page.function().windowSize()        
            .url(url_1)
            .pause(500)
            .element('css selector', 'title', function(result){
                if (result.value && result.value.ELEMENT) {
                    browser
                    .assert.urlEquals(url_1, 'url_1 - Page url equals url_1')
                    .pause(500)            
                    .waitForElementVisible('body', 2500, 'url_1 - Body was displayed')            
                    .saveScreenshot('./screenshots/url_1.jpeg')
                .end();
                } else {
                    // URL HAS NOT BEEN ENTERED INTO GLOBALS.JS
                    console.log(consoleWarnColor, "WARN:                                                      ")
                    console.log(consoleTextColor, "URL_1 - No url entered for #1 in 'globals.js' file         ")
                    console.log(consoleTextColor, "        Add a url to #7 and rerun this test to see results ")                                        
                    browser.end();
                }
            })
      .end();
    }
};