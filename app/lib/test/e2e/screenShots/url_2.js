module.exports = {
    "URL_2 SCREENSHOT..." : function (browser) {
        browser
            .page.function().windowSize()        
            .url(url_2)
            .pause(500)
            .element('css selector', 'title', function(result){
                if (result.value && result.value.ELEMENT) {
                    browser
                    .assert.urlEquals(url_2, 'url_2 - Page url equals url_2')
                    .pause(500)            
                    .waitForElementVisible('body', 2500, 'url_2 - Body was displayed')            
                    .saveScreenshot('./screenshots/url_2.jpeg')
                .end();
                } else {
                    // URL HAS NOT BEEN ENTERED INTO GLOBALS.JS
                    console.log(consoleWarnColor, "WARN:                                                      ")
                    console.log(consoleTextColor, "URL-2 - No url entered for #2 in 'globals.js' file         ")
                    console.log(consoleTextColor, "        Add a url to #7 and rerun this test to see results ")                                        
                    browser.end();
                }
            })
      .end();
    }
};