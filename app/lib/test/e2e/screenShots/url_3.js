module.exports = {
    "URL_3 SCREENSHOT..." : function (browser) {
        browser
            .page.function().windowSize()    
            .url(url_3)
            .pause(500)
            .element('css selector', 'title', function(result){
                if (result.value && result.value.ELEMENT) {
                    browser
                    .assert.urlEquals(url_3, 'url_3 - Page url equals url_3')
                    .pause(500)            
                    .waitForElementVisible('body', 2500, 'url_3 - Body was displayed')            
                    .saveScreenshot('./screenshots/url_3.jpeg')
                .end();
                } else {
                    // URL HAS NOT BEEN ENTERED INTO GLOBALS.JS
                    console.log(consoleWarnColor, "WARN:                                                      ")
                    console.log(consoleTextColor, "URL_3 - No url entered for #3 in 'globals.js' file         ")
                    console.log(consoleTextColor, "        Add a url to #7 and rerun this test to see results ")                                        
                    browser.end();
                }
            })
      .end();
    }
};